# WhatIP - WebExtension

This add-on for Firefox 57(+) is designed to replace the old [ShowIP](https://addons.mozilla.org/en-us/firefox/addon/showip/) add-on (and maybe [FlagFox](https://addons.mozilla.org/FR/firefox/addon/flagfox/) too).  

Done all by myself from scratch, still in alpha stage, feel free fork, report issues and suggest features. ;)  

## TODO

- [ ] Countryflag
- [X] ~~WhoIS shortcut~~

## ChangeList  

### 0.2a Hotfix 24/08/17
* **Fixed**: removed previously added unnecessary *webNavigaton* permission. (Tried stuff and forgot to remove it)
* *activeTab* permission was also unnecessary.
* **Fixed**: icon and IP not updated when local scheme such as *about:about*. IP is now set to "Undefined" in such cases.
* The domaintools lookup when clicking the icon is only performed if the IP of host if different from "Undefined".

### 0.2 22/08/17
* Code and style reformat by [yfdyh000](https://github.com/yfdyh000).
* webRequest event changed from *onCompleted* to *onHeadersReceived* by [gloomy-ghost](https://github.com/gloomy-ghost).
* **Fixed**: issue where icon icon disappeared after clicking a link.
* Now using a *map* to keep track of '*host*' and '*ip*' and '*ipProtocolVersion*' for each tabs.
* **New Feature**: when clicking icon, opens a tab to look up IP on domaintools.

### 0.1 17/08/17
* Initial version.
* Icons for IPv4 and IPv6.
* Shows IP as tooltip.
