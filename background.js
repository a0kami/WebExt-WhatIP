/********
 * Vars *
 ********/
// Whether to output log or not
let log = false;

// Regular expressions for IPs
const ipv4 = /(?:\d+\.){3}\d+/;
const ipv6 = /(?:[\w*]:)*:[\w*]/;

// Filter for webRequest
const requestFilter = {
  urls: ["<all_urls>"],
  types: ["main_frame"]
};

// Map of 'tabId': {'host', 'ip' and 'ipProtocolVersion'} objects to keep track of the tab informations
let tabsMap = new Map();

// URL for domaintools, I'll consider using other services and/or set it as an option, just thinking it's one of the bests for now (because IPv6)
const dmnTlsURL = "https://whois.domaintools.com/";

/*************
 * Functions *
 *************/
// Set the appropriate icon from the version string
function setAppropriatePageAction(tabId) {
  // Setting TITLE
  let ip = tabsMap.get(tabId).ip;
  browser.pageAction.setTitle({ tabId, title: ip });

  // Setting ICON
  const prefix = "icons/";
  const extension = ".png";
  let version = tabsMap.get(tabId).ipProtocolVersion;
  let path16 = [prefix, version, "-16", extension].join('');
  let path32 = [prefix, version, "-32", extension].join('');
  browser.pageAction.setIcon({
    tabId,
    path: {
      16: path16,
      32: path32
    }
  });
  
  // TODO: Setting procedural Popup ?
}

// Handlers
function handleRequest(requestInfo) {
  // Collecting vars to update tabId values in the tabsMap
  let tabId = requestInfo.tabId;
  let host = new URL(requestInfo.url).hostname;
  let ip = requestInfo.ip;
  // Ip Procotol Version
  let ipProtocolVersion = "vX"; // Default if unrecognized
  
  if (ipv4.test(ip)) {
    if (log)
      console.log("ipv4");
    ipProtocolVersion = "v4";
  }
  else if (ipv6.test(ip)) {
    if (log)
      console.log("ipv6");
    ipProtocolVersion = "v6";
  }
  
  // Setting or Updating tabsMap
  tabsMap.set(tabId, { host: host, ip: ip, ipProtocolVersion: ipProtocolVersion });

  // Toggle icon after setting it right
  setAppropriatePageAction(tabId);
  browser.pageAction.show(tabId);
}

function handleTabActivated(activeTabInfo) {
  // Toggle Icon so it keeps showing on active tab
  if (log)
    console.log("Toggle icon on active tab: " + activeTabInfo.tabId);
  browser.pageAction.show(activeTabInfo.tabId);
}

// I can't decide whether deleting keys of closed tabs to save memory
// or let the key in the map because opening closed tab again with CTRL + SHIFT + T breaks the map.
function handleTabRemoved(tabId) {
  // Removing key from tabsMap as the tab no longer exists
  tabsMap.delete(tabId);
}

function handleTabUpdated(tabId, changeInfo, tab) {
  let host = new URL(tab.url).hostname;
  if (log) {
	console.log("New host: " + host + "\nLast host: " + tabsMap.get(tabId).host );
    console.log("Keys in tabsMap: " + tabsMap.size)
    console.log(tabsMap.get(tabId)); // May be undefined at this point  
  }

  // Condition to limit times this function is called (no filter on tabUpdate)
  if (tab.status == "complete") {
    // If the Map contains the current tab
    if (tabsMap.has(tabId)) {
	  // If the host stays the same
      if (host == tabsMap.get(tabId).host)
        setAppropriatePageAction(tabId);
      else {
        tabsMap.set(tabId, { host: host, ip: "Undefined", ipProtocolVersion: "vX" });
		setAppropriatePageAction(tabId);
      }
    }
    // Showing icon anyway (default one when tab update)
    browser.pageAction.show(tabId);
  }
}

function handlePageActionClicked(tab) {
  // When the icon is clicked find IP on domaintools.
  if (log)
    console.log("Icon in tab " + tab.id + " has been clicked.");
  if (tabsMap.has(tab.id)) {
    let ipToLookUp = tabsMap.get(tab.id).ip;
	// Discard undefined
	if (ipToLookUp != "Undefined") {
      let urlToOpen = [dmnTlsURL, ipToLookUp].join('');
      let newTab = browser.tabs.create({
        active: true,
        url: urlToOpen
      });
    }
  }
}

/********
 * Main *
 ********/
// Listeners
browser.webRequest.onResponseStarted.addListener(handleRequest, requestFilter);
browser.tabs.onActivated.addListener(handleTabActivated);
//browser.tabs.onRemoved.addListener(handleTabRemoved);
browser.tabs.onUpdated.addListener(handleTabUpdated);
browser.pageAction.onClicked.addListener(handlePageActionClicked);